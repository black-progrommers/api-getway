package services

import (
	"fmt"

	"github.com/hospital/api-getway/config"
	b "github.com/hospital/api-getway/genproto/building"
	d "github.com/hospital/api-getway/genproto/disease"
	h "github.com/hospital/api-getway/genproto/hospital"
	p "github.com/hospital/api-getway/genproto/patient"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"
)

type IServiceManager interface {
	BuildingService() b.BuildServiceClient
	DiseaseService() d.DiseaseServiceClient
	HospitalService() h.HospitalServiceClient
	PatientService() p.PatientServiceClient
}

type serviceManager struct {
	buildingService b.BuildServiceClient
	diseaseService  d.DiseaseServiceClient
	hospitalService h.HospitalServiceClient
	patientService  p.PatientServiceClient
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	resolver.SetDefaultScheme("dns")

	connBuilding, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.BuildingServiceHost, conf.BuildingServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connDisease, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.DiseaseServiceHost, conf.DiseaseServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connHospital, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.HospitalServiceHost, conf.HospitalServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connPatient, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.PatientServiceHost, conf.PatientServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	serviceManager := &serviceManager{
		buildingService: b.NewBuildServiceClient(connBuilding),
		diseaseService:  d.NewDiseaseServiceClient(connDisease),
		hospitalService: h.NewHospitalServiceClient(connHospital),
		patientService: p.NewPatientServiceClient(connPatient),
	}

	return serviceManager, err
}

func (s *serviceManager) BuildingService() b.BuildServiceClient {
	return s.buildingService
}

func (s *serviceManager) DiseaseService() d.DiseaseServiceClient {
	return s.diseaseService
}

func (s *serviceManager) HospitalService() h.HospitalServiceClient {
	return s.hospitalService
}

func (s *serviceManager) PatientService() p.PatientServiceClient {
	return s.patientService
}