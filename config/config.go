package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment string // develop, staging, production
	
	// context timeout in seconds
	CtxTimeout int

	LogLevel string
	HTTPPort string
	
	BuildingServiceHost string
	BuildingServicePort string
	DiseaseServiceHost  string
	DiseaseServicePort  string
	HospitalServiceHost string
	HospitalServicePort string
	PatientServiceHost string
	PatientServicePort string
}

func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))
	
	c.HospitalServiceHost = cast.ToString(getOrReturnDefault("HOSPITAL_SERVICE_HOST", "localhost"))
	c.HospitalServicePort = cast.ToString(getOrReturnDefault("HOSPITAL_SERVICE_PORT", ":8000"))

	c.BuildingServiceHost = cast.ToString(getOrReturnDefault("BUILDING_SERVICE_HOST", "localhost"))
	c.BuildingServicePort = cast.ToString(getOrReturnDefault("BUILDING_SERVICE_PORT", "8010"))

	c.DiseaseServiceHost = cast.ToString(getOrReturnDefault("DISEASE_SERVICE_HOST", "localhost"))
	c.DiseaseServicePort = cast.ToString(getOrReturnDefault("DISEASE_SERVICE_PORT", "8020"))

	c.PatientServiceHost = cast.ToString(getOrReturnDefault("PATIENT_SERVICE_HOST", "localhost"))
	c.PatientServicePort = cast.ToString(getOrReturnDefault("PATIENT_SERVICE_PORT", "8030"))


	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
