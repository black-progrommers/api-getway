package api

import (
	"github.com/hospital/api-getway/config"
	"github.com/hospital/api-getway/pkg/logger"
	"github.com/hospital/api-getway/services"

	//"github.com/gin-contrib/cors"
	_ "github.com/hospital/api-getway/api/docs"
	v1 "github.com/hospital/api-getway/api/handlers/v1"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
)

type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
}

// @title           Swagger for hospital api
// @version         1.0
// @description     This is a hospital service api.
// @BasePath  /v1
// @in header
func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
	})

	api := router.Group("/v1")

	// hospitals
	api.POST("/hospitals", handlerV1.CreateHospital)
	api.GET("/hospital/:id", handlerV1.GetHospitalById)
	api.GET("/hospitals", handlerV1.GetAllHospitals)
	api.GET("/hospitals/search", handlerV1.SearchHospitals)
	api.PUT("/hospital/:id", handlerV1.UpdateHospital)
	api.DELETE("/hospital/:id", handlerV1.DeleteHospital)


	url := ginSwagger.URL("swagger/doc.json")
	api.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}
