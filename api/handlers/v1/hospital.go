package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/hospital/api-getway/api/handlers/models"
	pu "github.com/hospital/api-getway/genproto/hospital"
	l "github.com/hospital/api-getway/pkg/logger"
	"github.com/hospital/api-getway/pkg/utils"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary create hospital
// @Description This api create a hospital
// @Tags Hospital
// @Accept json
// @Produce json
// @Param body body models.HospitalRequest true "Create Hospital"
// @Success 200 {object} models.HospitalResponse
// @Failure 400 {object} models.StandartErrorModel
// @Failure 500 {object} models.StandartErrorModel
// @Router /hospitals [post]
func (h *handlerV1) CreateHospital(c *gin.Context) {
	var (
		body        models.HospitalRequest
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json: ", l.Error(err))
		return
	}

	response, err := h.serviceManager.HospitalService().CreateHospital(context.Background(), &pu.HospitalRequest{
		Name: body.Name,
		Type: body.Type,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create hospital", l.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// @Summary update hospital
// @Description This api updates a hospital
// @Tags Hospital
// @Accept json
// @Produce json
// @Param body body models.UpdateHospitalRequest true "Update Hospital"
// @Success 200 {object} models.HospitalResponse
// @Failure 400 {object} models.StandartErrorModel
// @Failure 404 {object} models.StandartErrorModel
// @Failure 500 {object} models.StandartErrorModel
// @Router /hospital/{id} [put]
func (h *handlerV1) UpdateHospital(c *gin.Context) {
	var (
		body        models.UpdateHospitalRequest
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json: ", l.Error(err))
		return
	}

	response, err := h.serviceManager.HospitalService().UpdateHospital(context.Background(), &pu.UpdateHospitalRequest{
		Name: body.Name,
		Type: body.Type,
		Id:   body.Id,
	})

	if err != nil {
		if status.Code(err) == codes.NotFound {
			c.JSON(http.StatusNotFound, gin.H{
				"error": "hospital not found",
			})
			h.log.Error("hospital not found", l.Error(err))
			return
		}

		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update hospital", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.HospitalResponse{
		Id:        response.Id,
		Name:      response.Name,
		Type:      response.Type,
		CreatedAt: response.CreatedAt,
		UpdatedAt: response.UpdatedAt,
	})
}

// @Summary get hospital by id
// @Description This api gets a hospital by id
// @Tags Hospital
// @Accept json
// @Produce json
// @Param id path string true "Id"
// @Success 200 {object} models.HospitalResponse
// @Failure 400 {object} models.StandartErrorModel
// @Failure 500 {object} models.StandartErrorModel
// @Router /hospital/{id} [get]
func (h *handlerV1) GetHospitalById(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to get hospital by id: ", l.Error(err))
		return
	}

	response, err := h.serviceManager.HospitalService().GetHospitalById(context.Background(), &pu.IdRequest{Id: int64(id)})
	if err != nil {
		statusCode := http.StatusInternalServerError
		if status.Code(err) == codes.NotFound {
			statusCode = http.StatusNotFound
		}
		c.JSON(statusCode, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get hospital by id: ", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary get all hospitals
// @Description This api gets all hospitals
// @Tags Hospital
// @Accept json
// @Produce json
// @Param limit query int true "Limit"
// @Param page query int true "Page"
// @Success 200 {object} []models.HospitalResponse
// @Failure 400 {object} models.StandartErrorModel
// @Failure 500 {object} models.StandartErrorModel
// @Router /hospitals [get]
func (h *handlerV1) GetAllHospitals(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	params, errstr := utils.ParseQueryParams(queryParams)
	if errstr != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": errstr[0],
		})
		h.log.Error("Failed to get all hospitals: " + errstr[0])
		return
	}

	response, err := h.serviceManager.HospitalService().GetAllHospitals(context.Background(), &pu.GetAllRequest{
		Limit: params.Limit,
		Page:  params.Page,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to get all hospitals: ", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Summary delete hospital
// @Description This api deletes a hospital
// @Tags Hospital
// @Accept json
// @Produce json
// @Param id path int true "Id"
// @Success 200 {object} models.HospitalResponse
// @Failure 400 {object} models.StandartErrorModel
// @Failure 500 {object} models.StandartErrorModel
// @Router /hospital/{id} [delete]
func (h *handlerV1) DeleteHospital(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid hospital ID",
		})
		h.log.Error("Failed to parse hospital ID: ", l.Error(err))
		return
	}

	response, err := h.serviceManager.HospitalService().DeleteHospital(context.Background(), &pu.IdRequest{
		Id: id,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to delete hospital", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.HospitalResponse{
		Id:        response.Id,
		Name:      response.Name,
		Type:      response.Type,
		CreatedAt: response.UpdatedAt,
	})
}

// @Summary search hopitals by name
// @Description This api searches for hospitals by name
// @Tags Hospital
// @Accept json
// @Produce json
// @Param first_name query string true "Search"
// @Success 200 {object}  models.HospitalResponse
// @Failure 400 {object} models.StandartErrorModel
// @Failure 500 {object} models.StandartErrorModel
// @Router /hospitals/search [get]
func (h *handlerV1) SearchHospitals(c *gin.Context) {

	queryParams := c.Request.URL.Query()
	params, strerr := utils.ParseQueryParams(queryParams)

	if strerr != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": strerr[0],
		})
		h.log.Error("Failed to get all hospitals: " + strerr[0])
		return
	}

	response, err := h.serviceManager.HospitalService().SearchHospitalsByName(context.Background(), &pu.SearchHospitals{
		Search: params.Search,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to search hospitals: ", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}
