package models

type HospitalRequest struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type HospitalResponse struct {
	Id        int64  `json:"id"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type IdHospitalRequest struct {
	Id int64 `json:"id"`
}

type GetAllHospitalsRequest struct {
	Limit int64 `json:"limit"`
	Page  int64 `json:"page"`
}

type UpdateHospitalRequest struct {
	Id    int64  `json:"id"`
	Name  string `json:"first_name"`
	Type  string `json:"last_name"`
	Email string `json:"email"`
}

type Hospitals struct {
	Hospitals []HospitalResponse `json:"hospitals"`
}

type RegisterModel struct {
	Name string `json:"name"`
	Type  string `json:"type"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}
